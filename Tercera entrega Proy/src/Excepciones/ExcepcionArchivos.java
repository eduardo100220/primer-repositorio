/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excepciones;

import java.io.IOException;

/**
 *
 * @author alejandro brito
 */
public class ExcepcionArchivos extends IOException {

    public ExcepcionArchivos() {
        super("el archivo no es soportado o no tiene permisos");
    }
}

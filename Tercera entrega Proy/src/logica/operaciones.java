/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author alejandro brito
 */
public class operaciones {
    
    static String textoO= "";
    static String textoM="";
    static String palin = "", noPalin = "", conv = "";

    public static String getPalin() {
        return palin;
    }

    public static String getNoPalin() {
        return noPalin;
    }

    public static String getConv() {
        return conv;
    }
    
    
    
    public static String sacarPalabras(String fraseC,int nPal, String[]palabs,int pos)
    {
 
        
        if(pos<nPal)
        {
            if(palabs[pos].length()>=4)
            {
            fraseC += " " + arreglarPalab(palabs[pos]);
            return sacarPalabras(fraseC, nPal, palabs, ++pos); 
            }else{ 
                return sacarPalabras(fraseC, nPal, palabs, ++pos);
            }
        }else{
              //fraseC = fraseC + palabs[pos];
              return fraseC;
            }
        
    }
    
    public static String arreglarPalab (String pal)
    {
        
         String palOriginal;
        String palConv;
        palOriginal = pal;
        pal= pal.toLowerCase();
        pal=pal.replaceAll(",", "");
        pal=pal.replaceAll("\\.", "");
        pal=pal.replaceAll("á", "a");
        pal=pal.replaceAll("é", "e");
        pal=pal.replaceAll("í", "i");
        pal=pal.replaceAll("ó", "o");
        pal=pal.replaceAll("ú", "u");
        pal=pal.replaceAll("-", "");
        pal=pal.replaceAll("1", "");
        pal=pal.replaceAll("2", "");
        pal=pal.replaceAll("3", "");
        pal=pal.replaceAll("4", "");
        pal=pal.replaceAll("5", "");
        pal=pal.replaceAll("6", "");
        pal=pal.replaceAll("7", "");
        pal=pal.replaceAll("8", "");
        pal=pal.replaceAll("9", "");
        pal=pal.replaceAll("0", "");
        pal=pal.replaceAll("\\(", "");
        pal=pal.replaceAll("\\)", "");
        pal=pal.replaceAll("\\+", "");
        pal=pal.replaceAll("\\*", "");
        pal=pal.replaceAll("/", "");
        pal=pal.replaceAll("&", "");
        pal=pal.replaceAll("$", "");
        pal=pal.replaceAll("\\¿", "");
        pal=pal.replaceAll("\\?", "");
        pal=pal.replaceAll("¡", "");
        pal=pal.replaceAll("!", "");
         palindromaIs(pal);
        if (palindromaIs(pal)==true)
        {
            palin += palOriginal + " - ";
            textoM += palOriginal; 
            
            return palOriginal ;
        }else if(puedeTransformar( 1, 1, pal, 0, false)==true)
        { 
            char [] aux = palOriginal.toCharArray();
            palConv= cambiarPal( 0, palOriginal, "", 0, pal.length()-1, 0, aux) ;
            textoM += palConv ;
            conv += pal + " - ";
            return palConv ;
        }else{
            noPalin += palOriginal + " - ";
            textoM += palOriginal; 
            return palOriginal;
        }
       
    }

    public static boolean palindromaIs (String palab)
    {  
        if(palab.length() <= 1){
         return true;
           }else if(palab.charAt(0) == palab.charAt(palab.length() - 1)){
             return palindromaIs(palab.substring(1,palab.length() - 1 ) );
               }else{
                 return false;
           }
    } 
   
    
    public static int letrasRep(String pal, String pal2,int n )
    {
        if (n>pal.length()-1){
         return 0;
          }else if(pal2.contains(String.valueOf(pal.charAt(n)))){
            return letrasRep(pal,pal2 , ++n);
             }else{
               pal2=pal2+String.valueOf(pal.charAt(n));
                return letrasRep(pal,pal2, ++n)+1;
               }
         }
        
   

    
     public static boolean puedeTransformar (int par, int impar, String palab ,int n, boolean resp )
     {
         if((resp||n!=0)) {
             return resp;
         }else if (((impar ==1 && par == ((letrasRep(palab,"" , 0))-1)))) {
           return puedeTransformar(par, impar,palab, 1, true );
         }else if (par == letrasRep(palab,"",0)) {
             return puedeTransformar(par, impar,palab, 1, true);
         }{ 
             return puedeTransformar(par, impar, palab, 1, false);
         }
    }
     
     public static String cambiarPal(int i,String pal, String recorridas,int x,int y, int cant, char[] aux) {
		if(i==pal.length()){
		   return charLet(aux,"",0);
                   }else if(AgruparLetras(pal,  0,pal.charAt(cant), recorridas)==1){
                        aux[(pal.length()/2)]=pal.charAt(i);
			recorridas+=String.valueOf(pal.charAt(i));
                         return cambiarPal(++i, pal, recorridas, x, y, cant, aux);
			 }else if((AgruparLetras(pal,  0,pal.charAt(cant), recorridas)%2)==0){
			       cant =(AgruparLetras(pal,  0, pal.charAt(cant),recorridas))/2;
			       aux=cambiarL(aux,pal.charAt(i),x,y,0,cant);		
			       recorridas+=String.valueOf(pal.charAt(i));
				return cambiarPal(++i, pal, recorridas, x+=cant, y-=cant, cant, aux);
				}else if((AgruparLetras(pal,  0,pal.charAt(cant), recorridas)%2)==1){
                                     aux[(pal.length()/2)]=pal.charAt(i);
				     cant =(AgruparLetras(pal, 0,pal.charAt(cant), recorridas))/2;
				     aux=cambiarL(aux,pal.charAt(i),x,y,0,cant);			
				     recorridas+=String.valueOf(pal.charAt(i));
                                      return cambiarPal(++i, pal, recorridas, x+=cant,y-=cant, cant, aux);
                                      }else if(AgruparLetras(pal,  0, pal.charAt(cant),recorridas)==0){
                                          return cambiarPal(++i, pal, recorridas, x, y, cant,  aux);
                                       }
                          return null;
}

	



	public static char[] cambiarL(char[] aux,char letra, int pos1, int pos2, int i, int cant) {

		if(i==cant){
    		return aux;
    	}else{
    		aux[pos1]=letra;
			aux[pos2]=letra;
			return cambiarL(aux, letra, ++pos1, --pos2, ++i, cant);
             }
	}

	public static String charLet(char[] aux, String palab, int i) {
		if(i==aux.length){
			return palab;
		}else{
			palab = palab + aux[i];
			return charLet(aux, palab, ++i);
		}
	}
        
        public static void crearArchivo (String ruta, String textoModi, String textoOri) throws Excepciones.ExcepcionNoGuardado
        {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(ruta));) {
            bw.write("\n" + "Texto original:" +"\n" + "\n" + textoOri + "\n" + "\n" + "Texto modificado:" + "\n" + "\n" + textoModi );
            bw.flush();
        } catch (Exception e) {
            System.out.println("Error E/S: " + e);
        }
        }
        
        public static String cargarArchivo (String ruta) throws  Excepciones.ExcepcionArchivos, IOException
        {
   
             FileReader fr = new FileReader (ruta);
             BufferedReader br = new BufferedReader(fr);

            String linea = br.readLine();
            textoO=linea;
            return linea;
        }
                
                
   public static int AgruparLetras(String pal,  int pos,char letra, String letras){
		if(pos==pal.length() || letras.contains(String.valueOf(letra))){
				return 0;
		}else{
		   if(pal.charAt(pos)==letra){
		      return 1 + AgruparLetras(pal,  ++pos,letra, letras);
		       }else{
		         return AgruparLetras(pal,  ++pos, letra, letras);
			}
		}
	}

	
}



package Hilo;

import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author alejandro brito
 */
public class Hilo extends Observable implements Runnable {
    
    private String [] palabras;
    
    public Hilo (String [] palabras){
       this.palabras = palabras;
    }
    
    public static boolean palindromaIs (String palab)
    {  
        if(palab.length() <= 1){
         return true;
           }else if(palab.charAt(0) == palab.charAt(palab.length() - 1)){
             return palindromaIs(palab.substring(1,palab.length() - 1 ) );
               }else{
                 return false;
           }
    } 
    public static String arreglarPalab (String pal)
    {
        
        String palOriginal;
        String palConv;
        palOriginal = pal;
        pal= pal.toLowerCase();
        pal=pal.replaceAll(",", "");
        pal=pal.replaceAll("\\.", "");
        pal=pal.replaceAll("á", "a");
        pal=pal.replaceAll("é", "e");
        pal=pal.replaceAll("í", "i");
        pal=pal.replaceAll("ó", "o");
        pal=pal.replaceAll("ú", "u");
        pal=pal.replaceAll("-", "");
        pal=pal.replaceAll("1", "");
        pal=pal.replaceAll("2", "");
        pal=pal.replaceAll("3", "");
        pal=pal.replaceAll("4", "");
        pal=pal.replaceAll("5", "");
        pal=pal.replaceAll("6", "");
        pal=pal.replaceAll("7", "");
        pal=pal.replaceAll("8", "");
        pal=pal.replaceAll("9", "");
        pal=pal.replaceAll("0", "");
        pal=pal.replaceAll("\\(", "");
        pal=pal.replaceAll("\\)", "");
        pal=pal.replaceAll("\\+", "");
        pal=pal.replaceAll("\\*", "");
        pal=pal.replaceAll("/", "");
        pal=pal.replaceAll("&", "");
        pal=pal.replaceAll("$", "");
        pal=pal.replaceAll("\\¿", "");
        pal=pal.replaceAll("\\?", "");
        pal=pal.replaceAll("¡", "");
        pal=pal.replaceAll("!", "");
        
     return pal; 
    }
  
    public static int letrasRep(String pal, String pal2,int n )
    {
        if (n>pal.length()-1){
         return 0;
          }else if(pal2.contains(String.valueOf(pal.charAt(n)))){
            return letrasRep(pal,pal2 , ++n);
             }else{
               pal2=pal2+String.valueOf(pal.charAt(n));
                return letrasRep(pal,pal2, ++n)+1;
               }
         }
        
   public static String cambiarPal(int i,String pal, String recorridas,int x,int y, int cant, char[] aux) {
		if(i==pal.length()){
		   return charLet(aux," ",0);
                   }else if(OrganizarLetras(pal,  0,pal.charAt(cant), recorridas)==1){
                        aux[(pal.length()/2)]=pal.charAt(i);
			recorridas+=String.valueOf(pal.charAt(i));
                         return cambiarPal(++i, pal, recorridas, x, y, cant, aux);
			 }else if((OrganizarLetras(pal,  0,pal.charAt(cant), recorridas)%2)==0){
			       cant =(OrganizarLetras(pal,  0,pal.charAt(cant), recorridas))/2;
			       aux=cambiarL(aux,pal.charAt(i),x,y,0,cant);		
			       recorridas+=String.valueOf(pal.charAt(i));
				return cambiarPal(++i, pal, recorridas, x+=cant, y-=cant, cant, aux);
				}else if((OrganizarLetras(pal,  0, pal.charAt(cant), recorridas)%2)==1){
                                     aux[(pal.length()/2)]=pal.charAt(i);
				     cant =(OrganizarLetras(pal,  0, pal.charAt(cant), recorridas))/2;
				     aux=cambiarL(aux,pal.charAt(i),x,y,0,cant);			
				     recorridas+=String.valueOf(pal.charAt(i));
                                      return cambiarPal(++i, pal, recorridas, x+=cant,y-=cant, cant, aux);
                                      }else if(OrganizarLetras(pal,  0,pal.charAt(cant), recorridas)==0){
                                          return cambiarPal(++i, pal, recorridas, x, y, cant,  aux);
                                       }
                          return null;
}


	public static char[] cambiarL(char[] aux,char letra, int pos1, int pos2, int i, int cant) {
		if(i==cant){
    		return aux;
                    }else{
    		      aux[pos1]=letra;
                      aux[pos2]=letra;
		       return cambiarL(aux, letra, ++pos1, --pos2, ++i, cant);
              }
	}

	public static String charLet(char[] aux, String palab, int i) {
		if(i==aux.length){
                 return palab;
		  }else{
		    palab = palab + aux[i];
		      return charLet(aux, palab, ++i);
		}
	}
        
        public static int OrganizarLetras(String pal,  int pos,char letra, String letras){
		if(pos==pal.length()){
                 }else if(letras.contains(String.valueOf(letra))){
		  return 0;
		   }else{
		     if(pal.charAt(pos)==letra){
		      return 1 + OrganizarLetras(pal,  ++pos,letra, letras);
		       }else{
		         return OrganizarLetras(pal,  ++pos, letra, letras);
		       }
		  }
                  return 0;
	}

    
     public static boolean puedeTransformar (int par, int impar, String palab ,int n, boolean resp )
     {
         if((resp||n!=0)) {
           return resp;
            }else if (((impar ==1 && par == ((letrasRep(palab,"" , 0))-1)))) {
             return puedeTransformar(par, impar,palab, 1, true );
              }else if (par == letrasRep(palab,"",0)) {
               return puedeTransformar(par, impar,palab, 1, true);
               }{ 
                return puedeTransformar(par, impar, palab, 1, false);
         }
    }
     
   

    @Override
    
    public void run() {
        
        String textoModificado = "";
        try{ 
            for (String pal : palabras){
                textoModificado = recorrerMatriz(pal, textoModificado);
                this.setChanged();
                this.notifyObservers (textoModificado);
                this.clearChanged();
                Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static String recorrerMatriz(String palabra, String texto) throws InterruptedException
    {
        if (palindromaIs(arreglarPalab(palabra))==true){
            Thread.sleep(1000);
            return texto += palabra + " ";
             }else if (puedeTransformar( 1, 1, arreglarPalab(palabra), 0, false)==true) {
               char[]aux = palabra.toCharArray();   
               Thread.sleep(1000);
               return texto += " " + cambiarPal(0, palabra, "", 0, palabra.length()-1, 0, aux);
                }else{
                 Thread.sleep(1000);
                 return texto += palabra + " ";
        }    
    }
}
        
      
    

    


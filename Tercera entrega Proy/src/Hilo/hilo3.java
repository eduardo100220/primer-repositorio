
package Hilo;

import static Hilo.hilo3.cargarArchivo;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import palindorma1.NewJFrame;

/**
 *
 * @author alejandro brito
 */
public class hilo3 extends Thread{

    String ruta;
    
    public hilo3 (String ruta)
    {
        this.ruta=ruta;
    }
    
    public static String [] cargarArchivo (String ruta) throws  Excepciones.ExcepcionArchivos, IOException
        {
   
             FileReader fr = new FileReader (ruta);
             BufferedReader br = new BufferedReader(fr);

            String linea = br.readLine();
            //textoO=linea;
            String[] textoOriginal = linea.split("\\s+");
            return textoOriginal;
        }
    
            
            
    @Override
    public void run() {
      String textoEntrega = "";
        try {
            for (int i = 0; i<cargarArchivo(ruta).length;++i)
            {
                try {
                     textoEntrega += cargarArchivo(ruta)[i] + " ";
                    Thread.sleep(1000);
                    NewJFrame.jTextField1.setText(textoEntrega);
                } catch (IOException ex) {
                    Logger.getLogger(hilo3.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(hilo3.class.getName()).log(Level.SEVERE, null, ex);
                }
            }   } catch (IOException ex) {
            Logger.getLogger(hilo3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
